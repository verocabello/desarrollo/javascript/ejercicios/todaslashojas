window.addEventListener("load",inicio);
let canvas;
let ctx;
let botones;

function inicio(){
	canvas = document.querySelector("canvas");
	ctx = canvas.getContext("2d");

    botones = document.querySelectorAll("div>ul>li");
    for(let c=0; c<botones.length; c++){
    	botones[c].addEventListener("click",function(event){
		//ctx.strokeStyle = event.target.getAttribute("data-color");
		ctx.fillStyle = event.target.getAttribute("data-color");
		});
	}

	//canvas.addEventListener("mousemove",mostrar);
	//canvas.addEventListener("mouseover",dentro);
	
	//canvas.addEventListener("mousemove",function(event){
		//ctx.lineTo(event.clientX-this.offsetLeft,event.clientY-this.offsetTop);
		//ctx.stroke();
	//	circulo(event.clientX-event.target.offsetLeft,event.clientY-event.target.offsetTop,5);
	//});

	canvas.addEventListener("mousemove",function(event){
		//ctx.lineTo(event.clientX-this.offsetLeft,event.clientY-this.offsetTop);
		//ctx.stroke();
		cuadrado(event.clientX-event.target.offsetLeft,event.clientY-event.target.offsetTop,5);
	});

	canvas.addEventListener("mouseover",function(event){
		ctx.beginPath();
		ctx.lineWidth=0.5; //cambia la anchura de la línea
		ctx.moveTo(event.clientX-event.target.offsetLeft,event.clientY-event.target.offsetTop);
	});

}

//function dentro(event){
//	ctx.beginPath();
//	ctx.lineWidth=0.5; //cambia la anchura de la línea
//	ctx.moveTo(event.clientX-event.target.offsetLeft,event.clientY-event.target.offsetTop);
//}

//function mostrar(event){
//	ctx.lineTo(event.clientX-this.offsetLeft,event.clienteY-this.offsetTop);
//	ctx.stroke();
//}

function circulo(x,y,r){
	ctx.beginPath();
	ctx.arc(x,y,r,0,2*Math.PI);
	ctx.fill();
}

function cuadrado(x,y,lado){
	ctx.fillRect(x,y,lado,lado);
}

